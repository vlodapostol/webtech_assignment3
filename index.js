
const FIRST_NAME = "Vlad";
const LAST_NAME = "Apostol";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name,surname,salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

    getDetails(){
        return `${this.name} ${this.surname} ${this.salary}`;
    }
}

class SoftwareEngineer extends Employee {
   constructor(name,surname,salary,experience){
       super(name,surname,salary);
       if(experience===undefined){
           this.experience='JUNIOR'
       } else {
           this.experience=experience;
       }
   }

    applyBonus(){
        if(this.experience.match(/JUNIOR|MIDDLE|SENIOR/)){
            if(this.experience==='JUNIOR'){
                return this.salary+0.1*this.salary;
            }
            if(this.experience==='MIDDLE'){
                return this.salary+0.15*this.salary;
            }
            if(this.experience==='SENIOR'){
                return this.salary+0.2*this.salary;
            }
        } else {
            return this.salary+0.1*this.salary;
        }
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

